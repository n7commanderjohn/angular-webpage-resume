import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { AppComponent } from './app.component';
import { SkillsComponent } from './skills/skills.component';
import { WorkexpComponent } from './workexp/workexp.component';
import { EducationComponent } from './education/education.component';
import { CertificationsComponent } from './certifications/certifications.component';
import { ReferencesComponent } from './references/references.component';
import { ContactinfoComponent } from './contactinfo/contactinfo.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { MessagesComponent } from './messages/messages.component';
import { SummarizePipe } from './cust_pipes/summarize.pipe';
import { LikeComponent } from './like/like.component';
import { OcticonDirective } from './cust_directives/octicon.directive';
import { SignupFormComponent } from './signup-form/signup-form.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupFormComponent,
    SkillsComponent,
    WorkexpComponent,
    EducationComponent,
    CertificationsComponent,
    ReferencesComponent,
    ContactinfoComponent,
    HomepageComponent,
    NavbarComponent,
    MessagesComponent,
    SummarizePipe,
    LikeComponent,
    OcticonDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
