import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Skill } from './skills/skill';
import { SKILLLIST } from './skills/skill-list';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const SKILLLIST2: Skill[] = SKILLLIST;
    return {SKILLLIST2};
  }
}
