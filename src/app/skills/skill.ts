export class Skill {
    id: number;
    type: string;
    name: string;
    likesCount: number;
}

export class SkillObj {
    constructor(protected type: string, protected skills: Skill[]) {}
}
