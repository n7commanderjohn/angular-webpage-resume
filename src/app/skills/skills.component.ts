import { Component, OnInit } from '@angular/core';
import { Skill, SkillObj } from './skill';
import { SkillListService } from './skill-list.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {
  Skills: Skill[]; Networking: Skill[]; Systems: Skill[];
  Programming: Skill[]; Software: Skill[];
  skillTypes: string[]; skillObjs: SkillObj[];

  constructor(private skillListService: SkillListService) { }

  ngOnInit() {
    this.getSkills();
    this.skillTypes = [
      'Networking', 'Systems', 'Programming', 'Software'
    ];
    this.skillObjs = [
      new SkillObj('Networking', this.Networking),
      new SkillObj('Systems', this.Systems),
      new SkillObj('Programming', this.Programming),
      new SkillObj('Software', this.Software)
    ];
  }

  getSkills(): void {
    this.skillListService.getSkills()
      .subscribe(Skills => this.Skills = Skills);
    this.skillListService.getSkillsOfType('Networking')
      .subscribe(Networking => this.Networking = Networking);
    this.skillListService.getSkillsOfType('Systems')
      .subscribe(Systems => this.Systems = Systems);
    this.skillListService.getSkillsOfType('Programming')
      .subscribe(Programming => this.Programming = Programming);
    this.skillListService.getSkillsOfType('Software')
      .subscribe(Software => this.Software = Software);
  }

  getSkillsOfType(type): void {
    if (type === 'All') {
      this.skillListService.getSkills()
        .subscribe(Skills => this.Skills = Skills);
    } else if (type === 'Networking') {
        this.skillListService.getSkillsOfType(type)
          .subscribe(Networking => this.Networking = Networking);
    } else if (type === 'Systems') {
        this.skillListService.getSkillsOfType(type)
          .subscribe(Systems => this.Systems = Systems);
    } else if (type === 'Programming') {
        this.skillListService.getSkillsOfType(type)
          .subscribe(Programming => this.Programming = Programming);
    } else if (type === 'Software') {
        this.skillListService.getSkillsOfType(type)
          .subscribe(Software => this.Software = Software);
    }
  }
}
