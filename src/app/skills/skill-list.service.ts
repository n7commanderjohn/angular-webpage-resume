import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { Skill } from './skill';
import { SKILLLIST } from './skill-list';
import { MessagesService } from '../messages/messages.service';

@Injectable({
  providedIn: 'root'
})
export class SkillListService {

  constructor(
    private http: HttpClient,
    public messagesService: MessagesService,
  ) { }

  getSkills(): Observable<Skill[]> {
    this.messagesService.clear();
    this.messagesService.add('SkillListService: Got Skill List');
    return of(SKILLLIST);
  }

  getSkillsOfType(type): Observable<Skill[]> {
    const array = [];
    for (const skill of SKILLLIST) {
      if (skill.type === type) {
        array.push(skill);
      }
    }
    return of(array);
  }
}
