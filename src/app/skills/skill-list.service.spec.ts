import { TestBed, inject } from '@angular/core/testing';

import { SkillListService } from './skill-list.service';

describe('SkillListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SkillListService]
    });
  });

  it('should be created', inject([SkillListService], (service: SkillListService) => {
    expect(service).toBeTruthy();
  }));
});
