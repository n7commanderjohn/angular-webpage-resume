import { Component, OnInit, Input } from '@angular/core';

import { NavBarLink, NAVBARLINKS } from './navbar-links';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input() title: string;

  linksObj: NavBarLink[];
  activePage = 'Home';

  constructor() { }

  ngOnInit() {
    this.linksObj = NAVBARLINKS;
  }

}
