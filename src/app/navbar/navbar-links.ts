export class NavBarLink {
    link: string;
    desc: string;
}

export const NAVBARLINKS: NavBarLink[] = [
    { link: 'Home', desc: 'Home'},
    { link: 'Skills', desc: 'Skills'},
    { link: 'WorkExp', desc: 'Work Experience'},
    { link: 'Education', desc: 'Education'},
    { link: 'Certs', desc: 'Certifications'},
    { link: 'Contact', desc: 'Contact Info & References'},
];
