import { Directive, HostListener, Input, OnInit, ElementRef, Renderer2 } from '@angular/core';

import * as octicons from 'octicons';

@Directive({
    selector: '[appOcticon]'
})
export class OcticonDirective implements OnInit {

    constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

    @Input('appOcticon') appOcticon: string; @Input('color') color: string;
    @Input('active') active: string; @Input('width') width: number;

    @HostListener('click') onClick() {
        const el: HTMLElement = this.elementRef.nativeElement;
        const icon: Node = el.firstChild;
        if (this.active === 'false') {
            this.active = 'true';
            this.renderer.setStyle(icon, 'color', 'lightblue');
            // this.renderer.setAttribute(icon, 'class', 'true')
        } else {
            this.active = 'false';
            this.renderer.setStyle(icon, 'color', 'gray');
            // this.renderer.setAttribute(icon, 'class', 'false')
        }
    }

    ngOnInit(): void {
        const el: HTMLElement = this.elementRef.nativeElement;
        el.innerHTML = octicons[this.appOcticon].toSVG();

        const icon: Node = el.firstChild;
        if (this.color) {
            this.renderer.setStyle(icon, 'color', this.color);
        }
        if (this.width) {
            this.renderer.setStyle(icon, 'width', this.width);
            this.renderer.setStyle(icon, 'height', '100%');
        }
        this.renderer.setStyle(icon, 'cursor', 'pointer');
    }

}
