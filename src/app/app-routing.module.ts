import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { CertificationsComponent } from './certifications/certifications.component';
import { ContactinfoComponent } from './contactinfo/contactinfo.component';
import { EducationComponent } from './education/education.component';
import { HomepageComponent } from './homepage/homepage.component';
import { SkillsComponent } from './skills/skills.component';
import { WorkexpComponent } from './workexp/workexp.component';

const routes: Routes = [
  { path: '', redirectTo: '/Home', pathMatch: 'full' },
  { path: 'Certs', component: CertificationsComponent },
  { path: 'Contact', component: ContactinfoComponent },
  { path: 'Education', component: EducationComponent },
  { path: 'Home', component: HomepageComponent },
  { path: 'Skills', component: SkillsComponent },
  { path: 'WorkExp', component: WorkexpComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
