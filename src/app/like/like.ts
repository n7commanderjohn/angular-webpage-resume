import { SKILLLIST } from '../skills/skill-list';

export class Like {
    constructor(
        protected id: number,
        protected likesCount: number,
        protected isActive: boolean,
        protected octColor2: string
    ) {}
}

export let LikeList: Like[] = [];

export function generateLikes(): void {
    SKILLLIST.forEach( skill =>
        LikeList.push(new Like (
            skill.id, skill.likesCount, false, 'red'
        ))
    );
}
