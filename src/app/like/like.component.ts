import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent implements OnInit {
  @Input('likesCount') likesCount: number;
  @Input('isActive') isActive: boolean;
  @Input('octColor2') octColor2: string;

  onClick() {
    this.likesCount += (this.isActive) ? -1 : 1; // increment/decrement
    this.isActive = !this.isActive; // toggle liked status
    // this.octColor2 = (this.octColor2 === 'red') ? 'green' : 'red';
  }
  constructor() { }

  ngOnInit() {
  }

}
