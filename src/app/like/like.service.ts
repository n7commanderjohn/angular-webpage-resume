import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Like, LikeList, generateLikes } from './like';
import { MessagesService } from '../messages/messages.service';

@Injectable({
  providedIn: 'root'
})
export class LikeService {

  constructor(
    public messagesService: MessagesService
  ) { }

  getLikes(): Observable<Like[]> {
    this.messagesService.clear();
    this.messagesService.add('LikeService: Got Like List');
    generateLikes(); // comment out when database is made
    return of(LikeList);
  }
}
