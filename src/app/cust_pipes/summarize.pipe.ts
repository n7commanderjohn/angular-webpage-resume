import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'summarize'
})
export class SummarizePipe implements PipeTransform {

  transform(value: string, limit?: number, addSumText?: string): any {
    if (!value) {
      return null;
    } else {
      const actualLimit = limit ? limit : 50;
      value = value.substr(0, actualLimit) + addSumText;
      return value;
    }
  }

}
