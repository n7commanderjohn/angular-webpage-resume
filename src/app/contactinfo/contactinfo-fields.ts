export class ContactFormFields {
    fieldID: string;
    labelContent: string;
    required: boolean;
    minLength: number;
    maxLength: number;
    colClass: string;
}

export const CONTACTFORMFIELDLIST: ContactFormFields[] = [
    { fieldID: 'firstName', labelContent: 'First Name',
      required: true, minLength: 2, maxLength: 20,
      colClass: 'col-md-3' },
    { fieldID: 'lastName', labelContent: 'Last Name',
      required: false, minLength: 2, maxLength: 20,
      colClass: 'col-md-3' },
    { fieldID: 'pEmail', labelContent: 'Primary Email',
      required: false, minLength: 2, maxLength: 120,
      colClass: 'col-md-3' },
    { fieldID: 'sEmail', labelContent: 'Secondary Email',
      required: false, minLength: 2, maxLength: 120,
      colClass: 'col-md-3' },
];

export class ContactMethod {
    id: number;
    name: string;
}

export const CONTACTMETHODS: ContactMethod[] = [
    { id: 1, name: 'Email' },
    { id: 2, name: 'Phone' },
];
