import { Component, OnInit } from '@angular/core';
import * as cif from './contactinfo-fields';

@Component({
  selector: 'app-contactinfo',
  templateUrl: './contactinfo.component.html',
  styleUrls: ['./contactinfo.component.css']
})
export class ContactinfoComponent implements OnInit {
  fieldsObjs1: cif.ContactFormFields[];
  contactMethods: cif.ContactMethod[];

  constructor() { }

  ngOnInit() {
    this.fieldsObjs1 = cif.CONTACTFORMFIELDLIST.slice(0, 4);
    this.contactMethods = cif.CONTACTMETHODS;
  }

  log(x) {
    console.log(x);
  }

  submit(f) {
    console.log(f);
  }
}
